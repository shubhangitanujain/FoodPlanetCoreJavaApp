package foodPlanet.Pojo;

public class users {
    
   private static  String userid;
   private static String password;
    private static String userType;
    private static String userName;
    private static String empId;

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        users.userName = userName;
    }

    public static String getEmpId() {
        return empId;
    }

    public static void setEmpId(String empId) {
        users.empId = empId;
    }

    public  static  void  setUserid(String userid) {
        users.userid = userid;
    }

    public static void setPassword(String password) {
        users.password = password;
    }

    public  static void setUserType(String userType) {
        users.userType = userType;
    }

    public static  String getUserid() {
        return userid;
    }

    public static String getPassword() {
        return password;
    }

    public static  String getUserType() {
        return userType;
    }
    public users(){
        
    }
    
    public users(String userid, String password, String userType) {
        this.userid = userid;
        this.password = password;
        this.userType = userType;
    }
   
    
}
