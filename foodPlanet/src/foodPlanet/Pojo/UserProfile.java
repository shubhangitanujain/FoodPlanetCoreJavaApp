package foodPlanet.Pojo;

public class UserProfile {
    private static String userMode;
    private static String userProfile;

    public static String getUserMode() {
        return userMode;
    }

    public static String getUserProfile() {
        return userProfile;
    }

    public static void setUserMode(String userMode) {
        UserProfile.userMode = userMode;
    }

    public static void setUserProfile(String userProfile) {
        UserProfile.userProfile = userProfile;
    }
    
}
