package foodPlanet.Dao;

import foodPlanet.dbutil.DBConnection;
import foodPlanet.Pojo.Order;
import foodPlanet.Pojo.OrderDetail;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class OrderDao {
    public static ArrayList<Order>getOrderByDate(Date StartDate ,Date endDate) throws SQLException{
         
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("select * from orders where ord_date between to_date (?,'yy-mm-dd') and to_date (?,'yy-mm-dd') ");
        long ms1 = StartDate.getTime();
        long ms2 = endDate.getTime();
        java.sql.Date sdate = new java.sql.Date(ms1);
        java.sql.Date edate = new java.sql.Date(ms2);
        //System.out.println("start date is "+sdate.toString()+" end date "+edate);
        ps.setDate(1, sdate);
        ps.setDate(2, edate);
        ResultSet rs = ps.executeQuery();
       
        
        
        ArrayList<Order>ol = new ArrayList<Order>();
       
        while(rs.next()){
            //System.out.println("hello");
            Order ob = new Order();
            ob.setUserId(rs.getString("user_id"));
            ob.setOrderId(rs.getString("ord_id"));
            java.sql.Date d = rs.getDate("ord_date");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String dateStr = sdf.format(d);
            //System.out.println("date " +dateStr);
            ob.setOrderDate(dateStr);
            ob.setOrderAmount(rs.getDouble("ord_amount"));
            ob.setUserName(rs.getString("user_name"));
            ob.setGstAmount(rs.getDouble("gst_amount"));
            ob.setGst(rs.getDouble("gst"));
            ob.setGrandTotal(rs.getDouble("grand_total"));
            ob.setDiscount(rs.getDouble("discount"));
            ol.add(ob);
        
        }
        return ol;
        
        
        
        
        
    }
    public static ArrayList<Order>getOrder() throws SQLException{
         Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("select * from orders ");
        //long ms1 = StartDate.getTime();
        //long ms2 = StartDate.getTime();
        //java.sql.Date sdate = new java.sql.Date(ms1);
        //java.sql.Date edate = new java.sql.Date(ms2);
        //ps.setDate(1, sdate);
        //ps.setDate(2, edate
        ResultSet rs = ps.executeQuery();
        ArrayList<Order>ol = new ArrayList<Order>();
        while(rs.next()){
            Order ob = new Order();
            ob.setUserId(rs.getString("user_id"));
            ob.setOrderId(rs.getString("ord_id"));
            //ob.setOrderDate(rs.getString("ord_date"));
            ob.setOrderAmount(rs.getDouble("ord_amount"));
            java.sql.Date d = rs.getDate("ord_date");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String dateStr = sdf.format(d);
            ob.setOrderDate(dateStr);
            ob.setUserName(rs.getString("user_name"));
            ob.setGstAmount(rs.getDouble("gst_amount"));
            ob.setGst(rs.getDouble("gst"));
            ob.setGrandTotal(rs.getDouble("grand_total"));
            ob.setDiscount(rs.getDouble("discount"));
            ol.add(ob);
        
        }
        return ol;
        
    }
    public static String newOrderId() throws SQLException{
        Connection conn=DBConnection.getConnection();
        PreparedStatement ps=conn.prepareStatement("Select count(*) from Orders");
        int id=101;
        ResultSet rs=ps.executeQuery();
        if(rs.next())
        {
            id=id+rs.getInt(1);
         }
        return "OD"+id;

    } 
    public static int deleteOrder(String id) throws SQLException{
     Connection conn=DBConnection.getConnection();
        PreparedStatement ps=conn.prepareStatement("update orders set user_id = null where user_id = ?");
        ps.setString(1,id);
        int x = ps.executeUpdate();
        return x;
        
    }
    
    public static boolean addOrder(Order order,ArrayList<OrderDetail> orderList)throws Exception{
        Connection conn=DBConnection.getConnection();
        PreparedStatement ps=conn.prepareStatement("Insert into Orders values(?,?,?,?,?,?,?,?,?)");
        ps.setString(1, order.getOrderId());
        String dateStr=order.getOrderDate();
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d1=sdf.parse(dateStr);
        java.sql.Date d2=new java.sql.Date(d1.getTime());
        ps.setDate(2, d2);
        System.out.println("in add order "+d2);
        ps.setDouble(3,order.getGst());
        ps.setDouble(4, order.getGstAmount());
        ps.setDouble(5, order.getDiscount());
        ps.setDouble(6, order.getGrandTotal());
        ps.setString(7,order.getUserId());
        System.out.println("userId" +order.getUserId());
        ps.setString(8, order.getUserName());
        ps.setDouble(9,order.getOrderAmount());
        int x=ps.executeUpdate();
        PreparedStatement ps2=conn.prepareStatement("Insert into orderDetails values(?,?,?,?)");
        int count=0,y;
        for(OrderDetail detail:orderList)
        {
            ps2.setString(1, detail.getOrdId());
            ps2.setString(2, detail.getProdId());
            ps2.setDouble(3, detail.getQuantity());
            ps2.setDouble(4, detail.getCost());
            y=ps2.executeUpdate();
            count=count+y;
        }
        if(x>0 && count==orderList.size())
            return true;
        else
            return false;
    }

}
