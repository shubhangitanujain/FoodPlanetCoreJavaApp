
package foodPlanet.Dao;

import foodPlanet.dbutil.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class CategoryDao {
    
    public static String getNewID()throws SQLException
    {
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("Select count(*) from categories");
        int id = 101;
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            id += rs.getInt(1);
        }
        return "c"+id;
    }
    public static HashMap<String,String> getAllcategory() throws SQLException{
       
        Connection conn = DBConnection.getConnection();
        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery("select cat_id,cat_name from categories");
        HashMap<String,String> categories = new HashMap<>();
        while(rs.next()){
            String catId = rs.getString(1);
            String catName = rs.getString(2);
            categories.put(catName,catId);
        }
        
        return categories;
    
    }
    public static HashMap<String,String> getAll() throws SQLException{
       
        Connection conn = DBConnection.getConnection();
        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery("select cat_name,cat_id from categories");
        HashMap<String,String> categories = new HashMap<>();
        while(rs.next()){
            String catName = rs.getString(1);
            String catId = rs.getString(2);
            categories.put(catId,catName);
        }
        
        return categories;
    
    }
    
    public static boolean updateCat(String catName,String catId) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("update categories set cat_name=? where cat_id=?");
        ps.setString(1, catName);
        ps.setString(2, catId);
        int x = ps.executeUpdate();
        
        return (x>0);
    }
    
    public static boolean addCat(String catName,String catId) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("insert into categories values(?,?)");
        ps.setString(1, catId);
        ps.setString(2, catName);
        int x = ps.executeUpdate();
        
        return (x>0);
    }
}
