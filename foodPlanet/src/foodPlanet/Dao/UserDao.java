
package foodPlanet.Dao;

import foodPlanet.dbutil.DBConnection;
import foodPlanet.Pojo.ProductPojo;
import foodPlanet.Pojo.users;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

public class UserDao {
    public static String getNewID()throws SQLException
        { 
        Statement st= DBConnection.getConnection().createStatement();
        String str= " select max(userid)  from users";

        ResultSet rs=st.executeQuery(str);
        int id=1;


            if(rs.next())
            {
                    String pid=rs.getString(1);
                    int pno=Integer.parseInt(pid.substring(1));
                    id=id+pno;

            String sr="u"+id;
            return sr;
        
            }
    else
    return "u101";
        
    }
    public static String mymeth(String id) throws SQLException{
      PreparedStatement ps = DBConnection.getConnection().prepareStatement("select ename from employee where empid=?");
        String user = null;
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            user = rs.getString(1);
            
        }
        return user;   
    }
    /**
     *
     * @return
     * @throws java.sql.SQLException
     */
    public static  HashMap<String,String> disp(String id) throws SQLException{
         //ArrayList<users> al = new ArrayList<>();
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("select username,empid from users where empid is not null and userid=?");
        HashMap<String,String> hs = new HashMap<>();
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            String username = rs.getString(1);
            String empid = rs.getString(2);
            hs.put(username, empid);
        }
        return hs;
        
    }
    public static boolean  del(String id) throws SQLException{
    
        //ArrayList<users> al = new ArrayList<>();
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("update  users set empid = null  where userid=? ");
        PreparedStatement ps1 = DBConnection.getConnection().prepareStatement("update  employee  set status = 'inactive'  where  ");
        //HashMap<String,String> hs = new HashMap<>();
        ps.setString(1, id);
        int x = ps.executeUpdate();
        return x>0;
        
    }
    public static boolean  delEmp(String id) throws SQLException{
         //ArrayList<users> al = new ArrayList<>();
        PreparedStatement ps = DBConnection.getConnection().prepareStatement(" update users set emp_id = null where emp_id = ? ");
        //HashMap<String,String> hs = new HashMap<>();
        ps.setString(1, id);
        int x = ps.executeUpdate();
        return x>0;
    }
    public static String validateUser(users u) throws SQLException{
        String userName = null;
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("select username from users where userid= ? and password= ? and usertype= ? and empid is not null ");
        String userid = u.getUserid();
        ps.setString(1,u.getUserid());
        ps.setString(2,u.getPassword());
        ps.setString(3,u.getUserType());
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
         userName = rs.getString(1);
         JOptionPane.showMessageDialog(null, userName,"welcome!",JOptionPane.INFORMATION_MESSAGE);
        }
        return userName;
    }
    
    public static boolean insert(String id,String userId,String name,String pswd) throws SQLException{
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("insert into users values(?,?,?,?,?)");
        ps.setString(1, userId);
        ps.setString(2, name);
        ps.setString(3, id);
        ps.setString(4, pswd);
        ps.setString(5, "cashier");
        int x  = ps.executeUpdate();
        return (x>0);
    }
}
