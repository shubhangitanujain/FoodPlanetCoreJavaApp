package foodPlanet.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import foodPlanet.dbutil.DBConnection;
import foodPlanet.Pojo.Employee;

public class EmployeeDao {
    public static String getNewID()throws SQLException
    {
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("Select max(empid) from employee");
        String id = "101";
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            id = rs.getString(1).substring(1);
            int x = Integer.parseInt(id)+1;
            id = String.valueOf(x);
        }
        return "E"+id;
    }
    
     public static boolean updateEmployee(String name, String job ,Double salary,String empId)throws SQLException
    {
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("update employee set ename=? , job = ? ,sal = ? where empid=? and empid is not null");
        ps.setString(1, name);
        ps.setString(2, job);
        ps.setDouble(3, salary);
        ps.setString(4, empId);
        //System.out.println(name + " " +job +" "+ salary +"  "+empId);
        int x = ps.executeUpdate();
         if(x==1)
             return true;
         else
             return false;
    }
     public static Employee SearchEmployee(String empId)throws SQLException
    {
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("select * from employee where empid=? and status='active'");
        ps.setString(1, empId);
        ResultSet rs = ps.executeQuery();
        Employee e = new Employee();
       
        
        while(rs.next()){
            
            e.setEmpId(rs.getString("empid"));
            e.setEmpName(rs.getString("ename"));
            e.setJob(rs.getString("job"));
            e.setSal(rs.getDouble("sal"));
        }
        //System.out.print(rs.getDouble("sal"));
        
        return e;
    }
     public static ArrayList returnJob()throws SQLException
    {
    PreparedStatement ps = DBConnection.getConnection().prepareStatement("select job from employee");
    ResultSet rs = ps.executeQuery();
     ArrayList <String> List = new ArrayList<>();   
    while(rs.next()){
        List.add(rs.getString(1));
    }
    return List;
    }
     
     public static int deleteEmp(String id) throws SQLException{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement("update employee set status = 'inactive' and where empid = ?");
            PreparedStatement ps1 = DBConnection.getConnection().prepareStatement("update users set empid = null where empid = ?");
            ps.setString(1, id);
            ps1.setString(1, id);
            int  rs = ps.executeUpdate();
            int r = ps1.executeUpdate();
            return rs;
   
     }
     
     public static boolean addEmployee(Employee e)throws SQLException
    {
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("Insert into employee values (?,?,?,?,'active')");
        ps.setString(1, e.getEmpId());
        ps.setString(2, e.getEmpName());
        ps.setString(3, e.getJob());
        ps.setDouble(4, e.getSal());
        int x = ps.executeUpdate();
        return (x>0);
    }
    public static HashMap <String,Employee> getEmployeesByEmpId()throws SQLException
    {
        Statement st = DBConnection.getConnection().createStatement();
        HashMap <String,Employee> employeeList = new HashMap<>();
        ResultSet rs = st.executeQuery("select * from employee where status = 'active' and empid not in (select empid from users where empid is not null )");
        while(rs.next()){
            Employee e = new Employee();
            e.setEmpId(rs.getString("empid"));
            e.setEmpName(rs.getString("ename"));
            e.setJob(rs.getString("job"));
            e.setSal(rs.getDouble("sal"));
            employeeList.put(e.getEmpId(), e);
        }
        return employeeList;
    }
    public static HashMap <String,Employee> NotNullgetEmployees()throws SQLException
    {
        Statement st = DBConnection.getConnection().createStatement();
        HashMap <String,Employee> employeeList = new HashMap<>();
        ResultSet rs = st.executeQuery(" select * from employee where status='active' ");
        while(rs.next()){
            Employee e = new Employee();
            e.setEmpId(rs.getString("empid"));
            e.setEmpName(rs.getString("ename"));
            e.setJob(rs.getString("job"));
            e.setSal(rs.getDouble("sal"));
            employeeList.put(e.getEmpId(), e);
        }
        return employeeList;
    }
    public static HashMap <String,Employee> getEmployees()throws SQLException
    {
        Statement st = DBConnection.getConnection().createStatement();
        HashMap <String,Employee> employeeList = new HashMap<>();
        ResultSet rs = st.executeQuery(" select * from employee where status= 'active' ");
        while(rs.next()){
            Employee e = new Employee();
            e.setEmpId(rs.getString("empid"));
            e.setEmpName(rs.getString("ename"));
            e.setJob(rs.getString("job"));
            e.setSal(rs.getDouble("sal"));
            employeeList.put(e.getEmpId(), e);
        }
        return employeeList;
    }
    public static ArrayList<Employee>getAllData()throws SQLException
    {
        Statement st = DBConnection.getConnection().createStatement();
        ResultSet rs = st.executeQuery("select * from employee where status='active' ");
        ArrayList <Employee> employeeList = new ArrayList<>();
        while(rs.next())
        {
            Employee e = new Employee();
            e.setEmpId(rs.getString("empid"));
            e.setEmpName(rs.getString("ename"));
            e.setJob(rs.getString("job"));
            e.setSal(rs.getDouble("sal"));
            employeeList.add(e);
        }
        return employeeList;
    }
}
