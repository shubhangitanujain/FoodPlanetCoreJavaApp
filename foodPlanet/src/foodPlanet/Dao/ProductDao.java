package foodPlanet.Dao;
import foodPlanet.dbutil.DBConnection;
import foodPlanet.Pojo.ProductPojo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class ProductDao {
    public static String getNewId() throws SQLException{
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("select count(*) from products");
        int id = 101;
        ResultSet rs = ps.executeQuery();
           if(rs.next()){
               id= id+rs.getInt(1);
            }
           return "P"+id;
        }
    public static boolean addProduct(ProductPojo p) throws SQLException{
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("insert into products values(?,?,?,?,?)");
        ps.setString(1,p.getProId());
        ps.setString(2, p.getCatId());
        ps.setString(3, p.getProdName());
        ps.setFloat(4, p.getProdPrice());
        ps.setString(5, p.getIsActive());
        int x  = ps.executeUpdate();
        if(x>0)
            return true;
        else{
            System.out.println("error");
            return false;
        }
    }
    
    public static boolean updateProduct(ProductPojo p) throws SQLException{
        try{
        PreparedStatement ps = DBConnection.getConnection().prepareStatement("update  products  set cat_id=?,prod_name=?,prod_price=?,active=? where prod_id=?");
        ps.setString(1,p.getCatId());
        ps.setString(2,p.getProdName());
        ps.setFloat(3,p.getProdPrice());
        ps.setString(4,p.getIsActive());
        ps.setString(5,p.getProId());
        int x = ps.executeUpdate();
        return(x>0);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
    public static HashMap<String,String> displayProd(String cat_id) throws SQLException {
        Connection conn = DBConnection.getConnection();
        PreparedStatement s = conn.prepareStatement("select prod_id,prod_name from products where cat_id=?");
        s.setString(1, cat_id);
        ResultSet rs = s.executeQuery();
        HashMap<String,String> products = new HashMap<>();
        while(rs.next()){
            String prodId = rs.getString(1);
            String prodName = rs.getString(2);
            products.put(prodId,prodName);
        }
        
        return products;
    
   }
    public static HashMap<String,Float> displayName(String prodId) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement s = conn.prepareStatement("select prod_name,prod_price from products where prod_id=?");
        s.setString(1, prodId);
        ResultSet rs = s.executeQuery();
        HashMap<String,Float> prices = new HashMap<>();
        while(rs.next()){
            String prod_name = rs.getString(1);
            float price = rs.getFloat(2);
            prices.put(prod_name,price);
        }
        return prices;
    }
    public static String displayActive(String prodId) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement s = conn.prepareStatement("select active from products where prod_id=?");
        s.setString(1, prodId);
        ResultSet rs = s.executeQuery();
        //HashMap<String,Float> prices = new HashMap<>();
        String prod_price ="";
        while(rs.next()){
             prod_price = rs.getString(1);
        }
        return prod_price;
    }
     public static HashMap<String,ProductPojo> getProductsByCategory(String catid) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("select * from products where cat_id = ?");
        ps.setString(1, catid);
        HashMap<String,ProductPojo> productList = new HashMap<>();
        ResultSet rs = ps.executeQuery();

        while(rs.next()){
            ProductPojo p = new ProductPojo();
            p.setCatId(catid);
            p.setProId(rs.getString("Prod_id"));
            p.setProdName(rs.getString("prod_Price"));
            p.setProdPrice(rs.getFloat("prod_price"));
            p.setIsActive(rs.getString("Active"));
            productList.put(getNewId(),p);
        }
        return productList;
    }
     public static ArrayList<ProductPojo> getAllData() throws SQLException {
         Connection conn = DBConnection.getConnection();
         Statement ps = conn.createStatement();
         ResultSet rs = ps.executeQuery("Select * from products ");
        ArrayList<ProductPojo> al = new ArrayList<>();
        while(rs.next()){
            ProductPojo pj = new ProductPojo();
            pj.setCatId(rs.getString("prod_id"));
            pj.setProId(rs.getString("cat_id"));
            pj.setProdName(rs.getString("prod_name"));
            pj.setProdPrice(rs.getFloat("prod_price"));
            pj.setIsActive(rs.getString("active"));
            al.add(pj);
        }
        return al;
     }
     public static boolean removeProduct(String prodId) throws SQLException{
       Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("update products set active='N' where prod_id=?");
        ps.setString(1, prodId);
        int x = ps.executeUpdate();
        return(x>0);
         
     }
     
     public static  HashMap<String,String> getActiveProduct(String catId) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("select prod_id,prod_name from products where cat_id=? and active='Y' ");
        ps.setString(1, catId);
        ResultSet rs = ps.executeQuery();
        HashMap<String,String> products = new HashMap<>();
        while(rs.next()){
            String prodId = rs.getString(1);
            String prodName = rs.getString(2);
            products.put(prodId,prodName);
        }
        
        return products;
    
     }
}
