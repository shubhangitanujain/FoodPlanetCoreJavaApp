package foodPlanet.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

public class DBConnection {
    private static Connection conn = null;
    static{
        try{
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url ="jdbc:oracle:thin:@localhost:1521:xe";
	    String username = "planetfood";
	    String password = "student";
            conn = DriverManager.getConnection(url, username, password);
            JOptionPane.showMessageDialog(null, "Connection sucessful","Sucesss!",JOptionPane.INFORMATION_MESSAGE);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"Connection failed","failed",JOptionPane.ERROR_MESSAGE );
            e.printStackTrace();
        }
    
    }
    public static Connection getConnection(){
        return conn;
    }
    public static void  closeConnection(){

    try{
        conn.close();
        System.out.println("connection closed");
    }
    catch(Exception e){
        e.printStackTrace();
   }
  }
}

