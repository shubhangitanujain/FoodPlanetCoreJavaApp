package foodPlanet.gui;

import foodPlanet.Pojo.UserProfile;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import foodPlanet.gui.ViewAllOrdersFrame;
import foodPlanet.gui.ViewAllDateOrdersFrame;

public class adminForm extends javax.swing.JFrame {
   
    /**
     * Creates new form adminForm
     */
    public adminForm() {
        initComponents();
        setLocationRelativeTo(null);
        label1.setText("welcome  "+UserProfile.getUserProfile());
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jrAddEmp = new javax.swing.JRadioButton();
        JrViewEmp = new javax.swing.JRadioButton();
        JrEditEmp = new javax.swing.JRadioButton();
        JrRemEmp = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        JrRegCash = new javax.swing.JRadioButton();
        JrRemCash = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        JrViewOrd = new javax.swing.JRadioButton();
        JrViewDate = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        JrAddCat = new javax.swing.JRadioButton();
        JrEditCat = new javax.swing.JRadioButton();
        JrViewCat = new javax.swing.JRadioButton();
        jPanel7 = new javax.swing.JPanel();
        JrAddPrd = new javax.swing.JRadioButton();
        JrEditPrd = new javax.swing.JRadioButton();
        JrViewPrd = new javax.swing.JRadioButton();
        JrRemovePrd = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        label1 = new javax.swing.JLabel();
        logout = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        doTask = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("PLANET FOOD ADMIN PANEL");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("make a choice");

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jrAddEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jrAddEmp.setForeground(new java.awt.Color(0, 51, 0));
        jrAddEmp.setText("add Emp");

        JrViewEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrViewEmp.setForeground(new java.awt.Color(0, 51, 0));
        JrViewEmp.setText("view Emp");
        JrViewEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JrViewEmpActionPerformed(evt);
            }
        });

        JrEditEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrEditEmp.setForeground(new java.awt.Color(0, 51, 0));
        JrEditEmp.setText("edit Emp");

        JrRemEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrRemEmp.setForeground(new java.awt.Color(0, 51, 0));
        JrRemEmp.setText("Remove emp");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(JrRemEmp)
                    .addComponent(JrEditEmp, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JrViewEmp, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jrAddEmp, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap(100, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jrAddEmp)
                .addGap(18, 18, 18)
                .addComponent(JrViewEmp)
                .addGap(28, 28, 28)
                .addComponent(JrEditEmp)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(JrRemEmp)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(0, 102, 102));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        JrRegCash.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrRegCash.setForeground(new java.awt.Color(0, 51, 0));
        JrRegCash.setText("register cashier");

        JrRemCash.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrRemCash.setForeground(new java.awt.Color(0, 51, 0));
        JrRemCash.setText("remove cashier");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(JrRegCash)
                    .addComponent(JrRemCash))
                .addContainerGap(77, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(JrRegCash)
                .addGap(33, 33, 33)
                .addComponent(JrRemCash)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(0, 102, 102));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        JrViewOrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrViewOrd.setForeground(new java.awt.Color(0, 51, 0));
        JrViewOrd.setText("view orders");

        JrViewDate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrViewDate.setForeground(new java.awt.Color(0, 51, 0));
        JrViewDate.setText("view database");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JrViewDate)
                    .addComponent(JrViewOrd))
                .addContainerGap(73, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(JrViewOrd)
                .addGap(18, 18, 18)
                .addComponent(JrViewDate)
                .addContainerGap(133, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(0, 102, 102));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        JrAddCat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrAddCat.setForeground(new java.awt.Color(0, 51, 0));
        JrAddCat.setText("Add Category");

        JrEditCat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrEditCat.setForeground(new java.awt.Color(0, 51, 0));
        JrEditCat.setText("Edit Category");

        JrViewCat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrViewCat.setForeground(new java.awt.Color(0, 51, 0));
        JrViewCat.setText("View Category");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JrViewCat)
                    .addComponent(JrEditCat)
                    .addComponent(JrAddCat))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(JrAddCat)
                .addGap(28, 28, 28)
                .addComponent(JrEditCat)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(JrViewCat)
                .addGap(24, 24, 24))
        );

        jPanel7.setBackground(new java.awt.Color(0, 102, 102));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        JrAddPrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrAddPrd.setForeground(new java.awt.Color(0, 51, 0));
        JrAddPrd.setText("Add product");

        JrEditPrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrEditPrd.setForeground(new java.awt.Color(0, 51, 0));
        JrEditPrd.setText("Edit product");

        JrViewPrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrViewPrd.setForeground(new java.awt.Color(0, 51, 0));
        JrViewPrd.setText("View product");

        JrRemovePrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JrRemovePrd.setForeground(new java.awt.Color(0, 51, 0));
        JrRemovePrd.setText("remove product");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JrRemovePrd)
                    .addComponent(JrViewPrd)
                    .addComponent(JrEditPrd)
                    .addComponent(JrAddPrd))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(JrAddPrd)
                .addGap(18, 18, 18)
                .addComponent(JrEditPrd)
                .addGap(18, 18, 18)
                .addComponent(JrViewPrd)
                .addGap(18, 18, 18)
                .addComponent(JrRemovePrd)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Emp Options");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Cashier options");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Report options");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("category Options");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Product options");

        label1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));

        logout.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        logout.setForeground(new java.awt.Color(255, 255, 255));
        logout.setText("LOGOUT");
        logout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoutMouseExited(evt);
            }
        });

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/manager.png"))); // NOI18N

        doTask.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        doTask.setText("Do task");
        doTask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doTaskActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 543, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(159, 159, 159))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(132, 132, 132)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 174, Short.MAX_VALUE)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(99, 99, 99))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(235, 235, 235)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(380, 380, 380)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 106, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(90, 90, 90)
                        .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(272, 272, 272)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(75, 75, 75))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(290, 290, 290)
                .addComponent(doTask, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(88, 88, 88))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(doTask, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 27, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JrViewEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JrViewEmpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JrViewEmpActionPerformed

    private void logoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseEntered
        logout.setForeground(Color.yellow);
        
    }//GEN-LAST:event_logoutMouseEntered

    private void logoutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseExited
        logout.setForeground(Color.YELLOW);
    }//GEN-LAST:event_logoutMouseExited

    private String getOption(){
         if(jrAddEmp.isSelected())
            return jrAddEmp.getText();
        else if(JrEditEmp.isSelected())
            return JrEditEmp.getText();
        else if(JrEditEmp.isSelected())
            return JrEditEmp.getText();
        else if(JrRemEmp.isSelected())
            return JrRemEmp.getText();
        else if(JrViewEmp.isSelected())
            return JrViewEmp.getText();
        else if(JrViewOrd.isSelected())
             return JrViewOrd.getText();
        else if(JrRegCash.isSelected())
            return JrRegCash.getText();
        else if(JrRemCash.isSelected())
             return JrRemCash.getText();
        else if(JrViewDate.isSelected())
            return JrViewDate.getText();
        else if(JrViewOrd.isSelected())
             return JrViewOrd.getText();
        else if(JrAddCat.isSelected())
             return JrAddCat.getText();
        else if(JrEditCat.isSelected())
            return JrEditCat.getText();
        else if(JrViewCat.isSelected())
             return JrViewCat.getText();
        else if(JrAddPrd.isSelected())
            return JrAddPrd.getText();
        else if(JrEditPrd.isSelected())
            return JrEditPrd.getText();
        else if(JrViewPrd.isSelected())
            
            return JrViewPrd.getText();
        else if(JrRemovePrd.isSelected())
            return JrRemovePrd.getText();
        else
            return null;
    }
    private void doTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doTaskActionPerformed
        // TODO add your handling code here:
        String task = getOption();
        if(task==null){
            JOptionPane.showMessageDialog(null,"please choose an option","Error",JOptionPane.ERROR_MESSAGE);
        }
        JFrame jf = null;
        try{
        if(task.equals("Add product")){
           jf = new AddProductFrame();
        }
        else if(task.equals("View product")){
           jf = new ViewProductsFrame();
        }
        else if(task.equals("Edit product")){
           jf = new EditProductFrame();
        }
        else if(task.equals("remove product")){
           jf = new RemoveProductFrame();
        }
        else if(task.equals("register cashier"))	
            jf=new RegisterCashierFrame();
        else if(task.equals("remove cashier"))
            jf=new RemoveCashierFrame();
        else if(task.equals("view database"))
            jf = new ViewAllDateOrdersFrame();
        else if(task.equals("view orders")){
            jf=new ViewAllOrdersFrame();
            ViewAllOrdersFrame.calling("admin");
        }
        else if(task.equals("view dateWise"))
        {
            jf=new ViewAllDateOrdersFrame();
            ViewAllDateOrdersFrame.calling("admin");
        }
        else if(task.equals("Add Category"))
            jf=new AddCategoryFrame();
        else if(task.equals("Edit Category"))
            jf=new EditCategoryFrame();
        else if(task.equals("View Category"))
            jf=new ViewAllCategoriesFrame();
        else if(task.equals("add Emp"))
            jf=new AddEmpFrame();
        else if(task.equals("edit Emp"))
            jf= new EditEmpFrame();
        else if(task.equals("Remove emp"))
            jf=new RemoveEmpFrame();
        else if(task.equals("view Emp"))
            jf=new ViewEmpFrame();
        
        jf.setVisible(true);
        
        this.dispose();
        
        }
        catch(Exception e){
            System.out.println(e);
        }
    }//GEN-LAST:event_doTaskActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        LoginFrame l = new LoginFrame();
        l.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void logoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutMouseClicked
        // TODO add your handling code here:
        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_logoutMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
    
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new adminForm().setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton JrAddCat;
    private javax.swing.JRadioButton JrAddPrd;
    private javax.swing.JRadioButton JrEditCat;
    private javax.swing.JRadioButton JrEditEmp;
    private javax.swing.JRadioButton JrEditPrd;
    private javax.swing.JRadioButton JrRegCash;
    private javax.swing.JRadioButton JrRemCash;
    private javax.swing.JRadioButton JrRemEmp;
    private javax.swing.JRadioButton JrRemovePrd;
    private javax.swing.JRadioButton JrViewCat;
    private javax.swing.JRadioButton JrViewDate;
    private javax.swing.JRadioButton JrViewEmp;
    private javax.swing.JRadioButton JrViewOrd;
    private javax.swing.JRadioButton JrViewPrd;
    private javax.swing.JButton doTask;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JRadioButton jrAddEmp;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel logout;
    // End of variables declaration//GEN-END:variables
}
